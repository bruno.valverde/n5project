﻿using Microsoft.EntityFrameworkCore;
using N5.PermissionsManagement.Domain;
using N5.PermissionsManagement.UnitOfWork.Interface;

namespace N5.PermissionsManagement.UnitOfWork.SqlServer.Repositories
{
    internal class PermissionAllocationRepository : GenericRepository<PermissionAllocation>, IPermissionAllocationRepository
    {
        private readonly PermissionManagementDbContext _dbContext;

        public PermissionAllocationRepository(PermissionManagementDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task AddAllocations(List<PermissionAllocation> allocations)
        {
            await _dbContext.AddRangeAsync(allocations);
        }

        public async Task<bool> AllocationExists(int userId, int permissionTypeId, int period)
        {
            return await _dbContext.PermissionAllocations.AnyAsync(q => q.EmployeeId == userId
                                       && q.PermissionTypeId == permissionTypeId
                                       && q.Period == period);
        }

        public async Task<List<PermissionAllocation>> GetPermissionAllocationsWithDetails()
        {
            var permissionAllocations = await _dbContext.PermissionAllocations
               .Include(q => q.PermisionType)
               .ToListAsync();
            return permissionAllocations;
        }

        public async Task<List<PermissionAllocation>> GetPermissionAllocationsWithDetails(int userId)
        {
            var permissionAllocations = await _dbContext.PermissionAllocations.Where(q => q.EmployeeId == userId)
               .Include(q => q.PermisionType)
               .ToListAsync();
            return permissionAllocations;
        }

        public async Task<PermissionAllocation> GetPermissionAllocationWithDetails(int id)
        {
            var permissionAllocation = await _dbContext.PermissionAllocations
               .Include(q => q.PermisionType)
               .FirstOrDefaultAsync(q => q.Id == id);

            return permissionAllocation;
        }

        public async Task<PermissionAllocation> GetUserAllocations(int userId, int PermissionTypeId)
        {
            return await _dbContext.PermissionAllocations.FirstOrDefaultAsync(q => q.EmployeeId == userId
                                        && q.PermissionTypeId == PermissionTypeId);
        }
    }
}
