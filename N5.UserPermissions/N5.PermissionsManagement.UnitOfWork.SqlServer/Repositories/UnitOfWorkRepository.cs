﻿using N5.PermissionsManagement.UnitOfWork.Interface;

namespace N5.PermissionsManagement.UnitOfWork.SqlServer.Repositories
{
    public class UnitOfWorkRepository : IUnitOfWorkRepository
    {
        private readonly PermissionManagementDbContext _context;
        private IPermissionAllocationRepository _permissionAllocationRepository;
        private IPermissionRequestRepository _permissionRequestRepository;
        private IPermissionTypeRepository _permissionTypeRepository;
        private IEmployeeRepository _employeeRepository;

        public UnitOfWorkRepository(PermissionManagementDbContext context)
        {
            _context = context;
        }

        public IPermissionAllocationRepository PermissionAllocationRepository => _permissionAllocationRepository ??= new PermissionAllocationRepository(_context);

        public IPermissionRequestRepository PermissionRequestRepository => _permissionRequestRepository ??= new PermissionRequestRepository(_context);

        public IPermissionTypeRepository PermissionTypeRepository => _permissionTypeRepository ??= new PermissionTypeRepository(_context);

        public IEmployeeRepository EmployeeRepository => _employeeRepository ??= new EmployeeRepository(_context);

        public void Dispose()
        {
            _context.Dispose();
            GC.SuppressFinalize(this);
        }

        public async Task Save()
        {
            await _context.SaveChangesAsync();
        }
    }
}
