﻿using Microsoft.EntityFrameworkCore;
using N5.PermissionsManagement.Domain;
using N5.PermissionsManagement.UnitOfWork.Interface;

namespace N5.PermissionsManagement.UnitOfWork.SqlServer.Repositories
{
    public class PermissionRequestRepository : GenericRepository<PermissionRequest>, IPermissionRequestRepository
    {
        private readonly PermissionManagementDbContext _dbContext;

        public PermissionRequestRepository(PermissionManagementDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task ChangeApprovalStatus(PermissionRequest permissionRequest, bool? ApprovalStatus)
        {
            permissionRequest.Approved = ApprovalStatus;
            _dbContext.Entry(permissionRequest).State = EntityState.Modified;
        }

        public async Task<List<PermissionRequest>> GetPermissionRequestsWithDetails()
        {
            var permissionRequests = await _dbContext.PermissionRequests
                .Include(q => q.PermissionType)
                .ToListAsync();
            return permissionRequests;
        }

        public async Task<List<PermissionRequest>> GetPermissionRequestsWithDetails(int userId)
        {
            var permissionRequests = await _dbContext.PermissionRequests.Where(q => q.RequestingEmployeeId == userId)
          .Include(q => q.PermissionType)
          .ToListAsync();
            return permissionRequests;
        }

        public async Task<PermissionRequest> GetPermissionRequestWithDetails(int id)
        {
            var permissionRequest = await _dbContext.PermissionRequests
                .Include(q => q.PermissionType)
                .FirstOrDefaultAsync(q => q.Id == id);

            return permissionRequest;
        }
    }
}
