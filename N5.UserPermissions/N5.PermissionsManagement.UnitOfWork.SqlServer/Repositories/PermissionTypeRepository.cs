﻿
using N5.PermissionsManagement.Domain;
using N5.PermissionsManagement.UnitOfWork.Interface;

namespace N5.PermissionsManagement.UnitOfWork.SqlServer.Repositories
{
    public class PermissionTypeRepository : GenericRepository<PermissionType>, IPermissionTypeRepository
    {
        private readonly PermissionManagementDbContext _dbContext;

        public PermissionTypeRepository(PermissionManagementDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }
    }
}
