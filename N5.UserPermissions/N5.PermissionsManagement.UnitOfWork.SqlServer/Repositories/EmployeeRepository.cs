﻿using Microsoft.EntityFrameworkCore;
using N5.PermissionsManagement.Domain;
using N5.PermissionsManagement.UnitOfWork.Interface;

namespace N5.PermissionsManagement.UnitOfWork.SqlServer.Repositories
{
    public class EmployeeRepository : GenericRepository<Employee>, IEmployeeRepository
    {
        private readonly PermissionManagementDbContext _dbContext;
        public EmployeeRepository(PermissionManagementDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Employee> GetEmployee(int employeeId)
        {
            var employee = await _dbContext.Employees
               .FirstOrDefaultAsync(q => q.EmployeeId == employeeId);

            return employee;
        }

        public async Task<List<Employee>> GetEmployees()
        {
            return await _dbContext.Employees.ToListAsync();
        }
    }
}
