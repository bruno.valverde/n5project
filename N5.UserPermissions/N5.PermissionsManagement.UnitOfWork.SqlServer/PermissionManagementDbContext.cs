﻿using Microsoft.EntityFrameworkCore;
using N5.PermissionsManagement.Domain;

namespace N5.PermissionsManagement.UnitOfWork.SqlServer
{
    public class PermissionManagementDbContext : AuditableDbContext
    {
        public PermissionManagementDbContext(DbContextOptions<PermissionManagementDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(PermissionManagementDbContext).Assembly);
        }

        public DbSet<PermissionRequest> PermissionRequests { get; set; }
        public DbSet<PermissionType> PermissionTypes { get; set; }
        public DbSet<PermissionAllocation> PermissionAllocations { get; set; }
        public DbSet<Employee> Employees { get; set; }
    }
}
