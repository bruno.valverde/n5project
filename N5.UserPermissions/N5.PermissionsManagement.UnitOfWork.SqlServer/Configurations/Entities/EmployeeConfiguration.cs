﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using N5.PermissionsManagement.Domain;

namespace N5.PermissionsManagement.UnitOfWork.SqlServer.Configurations.Entities
{
    public class EmployeeConfiguration : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.HasData(
                new Employee
                {
                    EmployeeId = 1,
                    FisrtName = "employeeFirstName",
                    LastName = "employeeLastName",
                    Email = "employee@n5.com",
                    EmailConfirmed = true,
                    PasswordHash = "7D9C61732398AE7A24BDEBE0CE1C82B064B3C5F8DD0D850E9F3C9CC80CCE980F",
                    EmployeeFile = "AR000123",
                    SectorId = 1,
                    StartWorkDate = DateTime.Now
                }
            );
        }
    }
}
