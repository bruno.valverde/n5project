﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using N5.PermissionsManagement.Domain;

namespace N5.PermissionsManagement.UnitOfWork.SqlServer.Configurations.Entities
{
    public class PermissionRequestConfiguration : IEntityTypeConfiguration<PermissionRequest>
    {
        public void Configure(EntityTypeBuilder<PermissionRequest> builder) { }
    }
}
