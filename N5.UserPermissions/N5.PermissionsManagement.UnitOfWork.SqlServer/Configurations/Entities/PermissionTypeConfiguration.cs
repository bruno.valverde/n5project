﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using N5.PermissionsManagement.Domain;

namespace N5.PermissionsManagement.UnitOfWork.SqlServer.Configurations.Entities
{
    internal class PermissionTypeConfiguration : IEntityTypeConfiguration<PermissionType>
    {
        public void Configure(EntityTypeBuilder<PermissionType> builder)
        {
            builder.HasData(
                new PermissionType
                {
                    Id = 1,
                    DefaultDays = 10,
                    Name = "Vacation"
                },
                new PermissionType
                {
                    Id = 2,
                    DefaultDays= 3,
                    Name = "Sick"
                }
            );
        }
    }
}
