﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using N5.PermissionsManagement.Domain;


namespace N5.PermissionsManagement.UnitOfWork.SqlServer.Configurations.Entities
{
    public class PermissionAllocationConfiguration : IEntityTypeConfiguration<PermissionAllocation>
    {
        public void Configure(EntityTypeBuilder<PermissionAllocation> builder)
        {
            builder.HasData(
                new PermissionAllocation
                {
                    Id = 1,
                    NumberOfDays = 15,
                    PermissionTypeId = 1,
                    Period = 2022,
                    EmployeeId = 1
                },
                new PermissionAllocation
                {
                    Id = 2,
                    NumberOfDays = 10,
                    PermissionTypeId = 2,
                    Period = 2022,
                    EmployeeId = 1
                }
            );
        }
    }
}
