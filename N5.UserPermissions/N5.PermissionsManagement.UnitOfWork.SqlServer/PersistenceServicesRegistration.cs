﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using N5.PermissionsManagement.UnitOfWork.Interface;
using N5.PermissionsManagement.UnitOfWork.Interface.Contracts;
using N5.PermissionsManagement.UnitOfWork.SqlServer.Repositories;

namespace N5.PermissionsManagement.UnitOfWork.SqlServer
{
    public static class PersistenceServicesRegistration
    {
        public static IServiceCollection ConfigurePersistenceServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<PermissionManagementDbContext>(options =>
               options.UseSqlServer(
                   configuration.GetConnectionString("PermissionManagementConnectionString")));


            services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));
            services.AddScoped<IUnitOfWorkRepository, UnitOfWorkRepository>();

            services.AddScoped<IPermissionTypeRepository, PermissionTypeRepository>();
            services.AddScoped<IPermissionRequestRepository, PermissionRequestRepository>();
            services.AddScoped<IPermissionAllocationRepository, PermissionAllocationRepository>();
            services.AddScoped<IEmployeeRepository, EmployeeRepository>();

            return services;
        }
    }
}
