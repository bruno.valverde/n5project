﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace N5.PermissionsManagement.UnitOfWork.SqlServer.Migrations
{
    public partial class addPkToEntities : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "EmployeeId",
                keyValue: 1,
                column: "StartWorkDate",
                value: new DateTime(2022, 5, 21, 19, 39, 54, 768, DateTimeKind.Local).AddTicks(4320));

            migrationBuilder.CreateIndex(
                name: "IX_PermissionTypes_Id",
                table: "PermissionTypes",
                column: "Id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PermissionRequests_Id",
                table: "PermissionRequests",
                column: "Id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PermissionAllocations_Id",
                table: "PermissionAllocations",
                column: "Id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Employees_EmployeeId",
                table: "Employees",
                column: "EmployeeId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_PermissionTypes_Id",
                table: "PermissionTypes");

            migrationBuilder.DropIndex(
                name: "IX_PermissionRequests_Id",
                table: "PermissionRequests");

            migrationBuilder.DropIndex(
                name: "IX_PermissionAllocations_Id",
                table: "PermissionAllocations");

            migrationBuilder.DropIndex(
                name: "IX_Employees_EmployeeId",
                table: "Employees");

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "EmployeeId",
                keyValue: 1,
                column: "StartWorkDate",
                value: new DateTime(2022, 5, 21, 19, 23, 21, 202, DateTimeKind.Local).AddTicks(3811));
        }
    }
}
