﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace N5.PermissionsManagement.UnitOfWork.SqlServer.Migrations
{
    public partial class SeedingTablesTest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "EmployeeId", "Email", "EmailConfirmed", "EmployeeFile", "FisrtName", "LastName", "PasswordHash", "SectorId", "StartWorkDate", "UserName" },
                values: new object[] { 1, "employee@n5.com", true, "AR000123", "employeeFirstName", "employeeLastName", "7D9C61732398AE7A24BDEBE0CE1C82B064B3C5F8DD0D850E9F3C9CC80CCE980F", 1, new DateTime(2022, 5, 21, 19, 14, 10, 920, DateTimeKind.Local).AddTicks(9708), "" });

            migrationBuilder.InsertData(
                table: "PermissionTypes",
                columns: new[] { "Id", "CreatedBy", "DateCreated", "DefaultDays", "LastModifiedBy", "LastModifiedDate", "Name" },
                values: new object[] { 1, null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 10, null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Vacation" });

            migrationBuilder.InsertData(
                table: "PermissionTypes",
                columns: new[] { "Id", "CreatedBy", "DateCreated", "DefaultDays", "LastModifiedBy", "LastModifiedDate", "Name" },
                values: new object[] { 2, null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Sick" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Employees",
                keyColumn: "EmployeeId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PermissionTypes",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PermissionTypes",
                keyColumn: "Id",
                keyValue: 2);
        }
    }
}
