﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace N5.PermissionsManagement.UnitOfWork.SqlServer.Migrations
{
    public partial class sqlDocker : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "EmployeeId",
                keyValue: 1,
                column: "StartWorkDate",
                value: new DateTime(2022, 5, 21, 23, 38, 45, 517, DateTimeKind.Local).AddTicks(4678));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "EmployeeId",
                keyValue: 1,
                column: "StartWorkDate",
                value: new DateTime(2022, 5, 21, 20, 2, 17, 499, DateTimeKind.Local).AddTicks(8422));
        }
    }
}
