﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace N5.PermissionsManagement.UnitOfWork.SqlServer.Migrations
{
    public partial class keymodification : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "EmployeeId",
                keyValue: 1,
                column: "StartWorkDate",
                value: new DateTime(2022, 5, 21, 19, 58, 32, 758, DateTimeKind.Local).AddTicks(7001));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "EmployeeId",
                keyValue: 1,
                column: "StartWorkDate",
                value: new DateTime(2022, 5, 21, 19, 39, 54, 768, DateTimeKind.Local).AddTicks(4320));
        }
    }
}
