﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace N5.PermissionsManagement.UnitOfWork.SqlServer.Migrations
{
    public partial class SeedingPermissionAllocation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "EmployeeId",
                keyValue: 1,
                column: "StartWorkDate",
                value: new DateTime(2022, 5, 21, 19, 23, 21, 202, DateTimeKind.Local).AddTicks(3811));

            migrationBuilder.InsertData(
                table: "PermissionAllocations",
                columns: new[] { "Id", "CreatedBy", "DateCreated", "EmployeeId", "LastModifiedBy", "LastModifiedDate", "NumberOfDays", "Period", "PermissionTypeId" },
                values: new object[] { 1, null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 15, 2022, 1 });

            migrationBuilder.InsertData(
                table: "PermissionAllocations",
                columns: new[] { "Id", "CreatedBy", "DateCreated", "EmployeeId", "LastModifiedBy", "LastModifiedDate", "NumberOfDays", "Period", "PermissionTypeId" },
                values: new object[] { 2, null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 10, 2022, 2 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "PermissionAllocations",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PermissionAllocations",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "EmployeeId",
                keyValue: 1,
                column: "StartWorkDate",
                value: new DateTime(2022, 5, 21, 19, 14, 10, 920, DateTimeKind.Local).AddTicks(9708));
        }
    }
}
