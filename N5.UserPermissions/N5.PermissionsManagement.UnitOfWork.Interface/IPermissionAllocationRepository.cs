﻿
using N5.PermissionsManagement.Domain;
using N5.PermissionsManagement.UnitOfWork.Interface.Contracts;

namespace N5.PermissionsManagement.UnitOfWork.Interface
{
    public interface IPermissionAllocationRepository : IGenericRepository<PermissionAllocation>
    {
        Task<PermissionAllocation> GetPermissionAllocationWithDetails(int id);
        Task<List<PermissionAllocation>> GetPermissionAllocationsWithDetails();
        Task<List<PermissionAllocation>> GetPermissionAllocationsWithDetails(int userId);
        Task<bool> AllocationExists(int userId, int permissionTypeId, int period);
        Task AddAllocations(List<PermissionAllocation> allocations);
        Task<PermissionAllocation> GetUserAllocations(int userId, int PermissionTypeId);
    }
}
