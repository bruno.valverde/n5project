﻿
using N5.PermissionsManagement.Domain;
using N5.PermissionsManagement.UnitOfWork.Interface.Contracts;

namespace N5.PermissionsManagement.UnitOfWork.Interface
{
    public interface IPermissionRequestRepository : IGenericRepository<PermissionRequest>
    {
        Task<PermissionRequest> GetPermissionRequestWithDetails(int id);
        Task<List<PermissionRequest>> GetPermissionRequestsWithDetails();
        Task<List<PermissionRequest>> GetPermissionRequestsWithDetails(int userId);
        Task ChangeApprovalStatus(PermissionRequest permissionRequest, bool? ApprovalStatus);
    }
}
