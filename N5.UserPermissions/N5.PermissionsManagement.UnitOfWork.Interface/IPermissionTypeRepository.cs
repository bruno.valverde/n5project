﻿
using N5.PermissionsManagement.Domain;
using N5.PermissionsManagement.UnitOfWork.Interface.Contracts;

namespace N5.PermissionsManagement.UnitOfWork.Interface
{
    public interface IPermissionTypeRepository : IGenericRepository<PermissionType>
    {
    }
}
