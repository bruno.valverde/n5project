﻿
namespace N5.PermissionsManagement.UnitOfWork.Interface
{
    public interface IUnitOfWorkRepository
    {
        IPermissionAllocationRepository PermissionAllocationRepository { get; }
        IPermissionRequestRepository PermissionRequestRepository { get; }
        IPermissionTypeRepository PermissionTypeRepository { get; }
        IEmployeeRepository EmployeeRepository { get; }
        Task Save();
    }
}
