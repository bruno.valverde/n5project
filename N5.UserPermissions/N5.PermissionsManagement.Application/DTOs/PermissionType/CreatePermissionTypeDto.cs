﻿
namespace N5.PermissionsManagement.Application.DTOs.PermissionType
{
    public class CreatePermissionTypeDto : IPermissionTypeDto
    {
        public string Name { get; set; }
        public int DefaultDays { get; set; }
    }
}
