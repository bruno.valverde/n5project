﻿using FluentValidation;

namespace N5.PermissionsManagement.Application.DTOs.PermissionType.Validators
{
    public class CreatePermissionTypeDtoValidator : AbstractValidator<CreatePermissionTypeDto>
    {
        public CreatePermissionTypeDtoValidator()
        {
            Include(new IPermissionTypeDtoValidator());
        }
    }
}
