﻿using FluentValidation;

namespace N5.PermissionsManagement.Application.DTOs.PermissionType.Validators
{
    public class UpdatePermissionTypeDtoValidator : AbstractValidator<PermissionTypeDto>
    {
        public UpdatePermissionTypeDtoValidator()
        {
            Include(new IPermissionTypeDtoValidator());

            RuleFor(p => p.Id).NotNull().WithMessage("{PropertyName} must be present");
        }
    }
}
