﻿using N5.PermissionsManagement.Application.DTOs.Common;
using N5.PermissionsManagement.Application.DTOs.Employee;
using N5.PermissionsManagement.Application.DTOs.PermissionType;

namespace N5.PermissionsManagement.Application.DTOs.PermissionRequest
{
    public class PermissionRequestDto : BaseDto
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public EmployeeDto Employee { get; set; }
        public int RequestingEmployeeId { get; set; }
        public PermissionTypeDto PermissionType { get; set; }
        public int PermissionTypeId { get; set; }
        public DateTime DateRequested { get; set; }
        public string? RequestComments { get; set; }
        public DateTime? DateActioned { get; set; }
        public bool? Approved { get; set; }
        public bool Cancelled { get; set; }
    }
}
