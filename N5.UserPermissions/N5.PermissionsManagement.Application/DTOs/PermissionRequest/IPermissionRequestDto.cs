﻿
namespace N5.PermissionsManagement.Application.DTOs.PermissionRequest
{
    public interface IPermissionRequestDto
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int PermissionTypeId { get; set; }
        public int EmployeeId { get; set; }
    }
}
