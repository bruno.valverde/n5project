﻿using N5.PermissionsManagement.Application.DTOs.Common;

namespace N5.PermissionsManagement.Application.DTOs.PermissionRequest
{
    public class UpdatePermissionRequestDto : BaseDto, IPermissionRequestDto
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int PermissionTypeId { get; set; }
        public string? RequestComments { get; set; }
        public bool Cancelled { get; set; }
        public int EmployeeId { get; set; }
    }
}
