﻿using N5.PermissionsManagement.Application.DTOs.Common;
using N5.PermissionsManagement.Application.DTOs.Employee;
using N5.PermissionsManagement.Application.DTOs.PermissionType;


namespace N5.PermissionsManagement.Application.DTOs.PermissionRequest
{
    public class PermissionRequestListDto : BaseDto
    {
        public EmployeeDto Employee { get; set; }
        public int RequestingEmployeeId { get; set; }
        public PermissionTypeDto PermissionType { get; set; }
        public DateTime DateRequested { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool? Approved { get; set; }
    }
}
