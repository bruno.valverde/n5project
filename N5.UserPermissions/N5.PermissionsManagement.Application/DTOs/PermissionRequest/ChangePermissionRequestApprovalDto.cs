﻿using N5.PermissionsManagement.Application.DTOs.Common;

namespace N5.PermissionsManagement.Application.DTOs.PermissionRequest
{
    public class ChangePermissionRequestApprovalDto : BaseDto
    {
        public bool Approved { get; set; }
    }
}
