﻿using FluentValidation;
using N5.PermissionsManagement.UnitOfWork.Interface;

namespace N5.PermissionsManagement.Application.DTOs.PermissionRequest.Validators
{
    public class CreatePermissionRequestDtoValidator : AbstractValidator<CreatePermissionRequestDto>
    {
        private readonly IPermissionTypeRepository _permissionTypeRepository;
        private readonly IEmployeeRepository _employeeRepository;

        public CreatePermissionRequestDtoValidator(IPermissionTypeRepository permissionTypeRepository, IEmployeeRepository employeeRepository)
        {
            _permissionTypeRepository = permissionTypeRepository;
            _employeeRepository = employeeRepository;

            Include(new IPermissionRequestDtoValidator(_permissionTypeRepository, _employeeRepository));
        }
    }
}
