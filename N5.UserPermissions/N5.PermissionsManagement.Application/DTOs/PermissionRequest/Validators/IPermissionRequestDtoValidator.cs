﻿using FluentValidation;
using N5.PermissionsManagement.UnitOfWork.Interface;

namespace N5.PermissionsManagement.Application.DTOs.PermissionRequest.Validators
{
    public class IPermissionRequestDtoValidator : AbstractValidator<IPermissionRequestDto>
    {
        private readonly IPermissionTypeRepository _permissionTypeRepository;
        private readonly IEmployeeRepository _employeeRepository;

        public IPermissionRequestDtoValidator(IPermissionTypeRepository permissionTypeRepository, IEmployeeRepository employeeRepository)
        {
            _permissionTypeRepository = permissionTypeRepository;
            _employeeRepository = employeeRepository;

            RuleFor(p => p.StartDate)
                .LessThan(p => p.EndDate).WithMessage("{PropertyName} must be before {ComparisonValue}");

            RuleFor(p => p.EndDate)
                .GreaterThan(p => p.StartDate).WithMessage("{PropertyName} must be after {ComparisonValue}");

            RuleFor(p => p.PermissionTypeId)
                .GreaterThan(0)
                .MustAsync(async (id, token) => {
                    var permissionTypeExists = await _permissionTypeRepository.Exists(id);
                    return permissionTypeExists;
                })
                .WithMessage("{PropertyName} does not exist.");

            RuleFor(p => p.EmployeeId)
                .GreaterThan(0).WithMessage("{PropertyName} must be before {ComparisonValue}")
                .MustAsync(async (id, token) =>
                {
                    var employeeExists = await _employeeRepository.Exists(id);
                    return employeeExists;
                }).WithMessage("{PropertyName} does not exist.");

        }
    }
}
