﻿using FluentValidation;
using N5.PermissionsManagement.UnitOfWork.Interface;


namespace N5.PermissionsManagement.Application.DTOs.PermissionAllocation.Validators
{
    internal class UpdatePermissionAllocationDtoValidator : AbstractValidator<UpdatePermissionAllocationDto>
    {
        private readonly IPermissionTypeRepository _permissionTypeRepository;

        public UpdatePermissionAllocationDtoValidator(IPermissionTypeRepository permissionTypeRepository)
        {
            _permissionTypeRepository = permissionTypeRepository;

            Include(new IPermissionAllocationDtoValidator(_permissionTypeRepository));

            RuleFor(p => p.Id).NotNull().WithMessage("{PropertyName} must be present");
        }
    }
}
