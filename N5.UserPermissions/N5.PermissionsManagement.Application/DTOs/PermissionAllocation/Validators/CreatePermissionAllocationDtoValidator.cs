﻿using FluentValidation;
using N5.PermissionsManagement.UnitOfWork.Interface;

namespace N5.PermissionsManagement.Application.DTOs.PermissionAllocation.Validators
{
    internal class CreatePermissionAllocationDtoValidator : AbstractValidator<CreatePermissionAllocationDto>
    {
        private readonly IPermissionTypeRepository _permissionTypeRepository;

        public CreatePermissionAllocationDtoValidator(IPermissionTypeRepository permissionTypeRepository)
        {
            _permissionTypeRepository = permissionTypeRepository;

            RuleFor(p => p.PermissionTypeId)
                .GreaterThan(0)
                .MustAsync(async (id, token) =>
                {
                    var permissionTypeExists = await _permissionTypeRepository.Exists(id);
                    return permissionTypeExists;
                })
                .WithMessage("{PropertyName} does not exist.");
        }
    }
}
