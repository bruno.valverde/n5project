﻿using FluentValidation;
using N5.PermissionsManagement.UnitOfWork.Interface;

namespace N5.PermissionsManagement.Application.DTOs.PermissionAllocation.Validators
{
    internal class IPermissionAllocationDtoValidator : AbstractValidator<IPermissionAllocationDto>
    {
        private readonly IPermissionTypeRepository _permissionTypeRepository;

        public IPermissionAllocationDtoValidator(IPermissionTypeRepository permissionTypeRepository)
        {
            _permissionTypeRepository = permissionTypeRepository;

            RuleFor(p => p.NumberOfDays)
                .GreaterThan(0).WithMessage("{PropertyName} must greater than {ComparisonValue}");

            RuleFor(p => p.Period)
                .GreaterThanOrEqualTo(DateTime.Now.Year).WithMessage("{PropertyName} must be after {ComparisonValue}");

            RuleFor(p => p.PermissionTypeId)
                .GreaterThan(0)
                .MustAsync(async (id, token) =>
                {
                    var permissionTypeExists = await _permissionTypeRepository.Exists(id);
                    return permissionTypeExists;
                })
                .WithMessage("{PropertyName} does not exist.");
        }
    }
}
