﻿
namespace N5.PermissionsManagement.Application.DTOs.PermissionAllocation
{
    public class CreatePermissionAllocationDto
    {
        public int PermissionTypeId { get; set; }
    }
}
