﻿using N5.PermissionsManagement.Application.DTOs.Common;
using N5.PermissionsManagement.Application.DTOs.Employee;
using N5.PermissionsManagement.Application.DTOs.PermissionType;

namespace N5.PermissionsManagement.Application.DTOs.PermissionAllocation
{
    public class PermissionAllocationDto : BaseDto
    {
        public int NumberOfDays { get; set; }
        public PermissionTypeDto PermissionType { get; set; }
        public EmployeeDto Employee { get; set; }
        public int EmployeeId { get; set; }
        public int PermissionTypeId { get; set; }
        public int Period { get; set; }
    }
}
