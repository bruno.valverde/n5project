﻿
namespace N5.PermissionsManagement.Application.DTOs.PermissionAllocation
{
    public interface IPermissionAllocationDto
    {
        public int NumberOfDays { get; set; }
        public int PermissionTypeId { get; set; }
        public int Period { get; set; }
    }
}
