﻿using N5.PermissionsManagement.Application.DTOs.Common;

namespace N5.PermissionsManagement.Application.DTOs.PermissionAllocation
{
    public class UpdatePermissionAllocationDto : BaseDto, IPermissionAllocationDto
    {
        public int NumberOfDays { get; set; }
        public int PermissionTypeId { get; set; }
        public int Period { get; set; }
    }
}
