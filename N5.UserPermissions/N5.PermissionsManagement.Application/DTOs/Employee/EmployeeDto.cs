﻿
namespace N5.PermissionsManagement.Application.DTOs.Employee
{
    public class EmployeeDto : IEmployeeDto
    {
        public int EmployeeId { get; set; }
        public string Email { get; set; } = string.Empty;
        public string FisrtName { get; set; } = string.Empty;
        public string Lastname { get; set; } = string.Empty;
        public string EmployeeFile { get; set; } = string.Empty;
    }
}
