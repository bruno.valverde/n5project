﻿
namespace N5.PermissionsManagement.Application.DTOs.Employee
{
    public interface IEmployeeDto
    {
        public int EmployeeId { get; set; }
        public string Email { get; set; }
        public string FisrtName { get; set; }
        public string Lastname { get; set; }
        public string EmployeeFile { get; set; }
    }
}
