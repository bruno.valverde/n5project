﻿using AutoMapper;
using N5.PermissionsManagement.Application.DTOs.Employee;
using N5.PermissionsManagement.Application.DTOs.PermissionAllocation;
using N5.PermissionsManagement.Application.DTOs.PermissionRequest;
using N5.PermissionsManagement.Application.DTOs.PermissionType;
using N5.PermissionsManagement.Domain;

namespace N5.PermissionsManagement.Application.Profiles
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            #region PermissionRequest Mappings
            CreateMap<PermissionRequest, PermissionRequestDto>().ReverseMap();
            CreateMap<PermissionRequest, PermissionRequestListDto>()
                .ForMember(dest => dest.DateRequested, opt => opt.MapFrom(src => src.DateCreated))
                .ReverseMap();
            CreateMap<PermissionRequest, CreatePermissionRequestDto>().ReverseMap();
            CreateMap<PermissionRequest, UpdatePermissionRequestDto>().ReverseMap();
            #endregion PermissionRequest

            #region PermissionAllocation Mappings
            CreateMap<PermissionAllocation, PermissionAllocationDto>().ReverseMap();
            CreateMap<PermissionAllocation, CreatePermissionAllocationDto>().ReverseMap();
            CreateMap<PermissionAllocation, UpdatePermissionAllocationDto>().ReverseMap();
            #endregion

            #region PermissionType Mappins
            CreateMap<PermissionType, PermissionTypeDto>().ReverseMap();
            CreateMap<PermissionType, CreatePermissionTypeDto>().ReverseMap();
            #endregion

            #region Employee Mappins
            CreateMap<Employee, EmployeeDto>().ReverseMap();
            #endregion
        }
    }
}
