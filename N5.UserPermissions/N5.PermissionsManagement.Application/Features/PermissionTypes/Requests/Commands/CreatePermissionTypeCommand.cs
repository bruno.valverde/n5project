﻿using MediatR;
using N5.PermissionsManagement.Application.DTOs.PermissionType;
using N5.PermissionsManagement.Application.Responses;

namespace N5.PermissionsManagement.Application.Features.PermissionTypes.Requests.Commands
{
    public class CreatePermissionTypeCommand : IRequest<BaseCommandResponse>
    {
        public CreatePermissionTypeDto PermissionTypeDto { get; set; }
    }
}
