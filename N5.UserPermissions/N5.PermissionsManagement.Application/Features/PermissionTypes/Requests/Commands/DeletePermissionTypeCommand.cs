﻿using MediatR;

namespace N5.PermissionsManagement.Application.Features.PermissionTypes.Requests.Commands
{
    public class DeletePermissionTypeCommand : IRequest
    {
        public int Id { get; set; }
    }
}
