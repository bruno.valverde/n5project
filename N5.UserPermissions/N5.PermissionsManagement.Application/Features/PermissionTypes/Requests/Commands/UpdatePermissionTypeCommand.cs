﻿using MediatR;
using N5.PermissionsManagement.Application.DTOs.PermissionType;

namespace N5.PermissionsManagement.Application.Features.PermissionTypes.Requests.Commands
{
    public class UpdatePermissionTypeCommand : IRequest<Unit>
    {
        public PermissionTypeDto PermissionTypeDto { get; set; }
    }
}
