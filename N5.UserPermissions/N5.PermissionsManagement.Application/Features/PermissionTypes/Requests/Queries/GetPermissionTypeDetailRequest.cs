﻿using MediatR;
using N5.PermissionsManagement.Application.DTOs.PermissionType;

namespace N5.PermissionsManagement.Application.Features.PermissionTypes.Requests.Queries
{
    public class GetPermissionTypeDetailRequest : IRequest<PermissionTypeDto>
    {
        public int Id { get; set; }
    }
}
