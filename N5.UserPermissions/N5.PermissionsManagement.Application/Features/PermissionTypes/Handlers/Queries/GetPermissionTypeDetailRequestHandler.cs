﻿using AutoMapper;
using MediatR;
using N5.PermissionsManagement.Application.DTOs.PermissionType;
using N5.PermissionsManagement.Application.Features.PermissionTypes.Requests.Queries;
using N5.PermissionsManagement.UnitOfWork.Interface;

namespace N5.PermissionsManagement.Application.Features.PermissionTypes.Handlers.Queries
{
    public class GetPermissionTypeDetailRequestHandler : IRequestHandler<GetPermissionTypeDetailRequest, PermissionTypeDto>
    {
        private readonly IPermissionTypeRepository _permissionTypeRepository;
        private readonly IMapper _mapper;

        public GetPermissionTypeDetailRequestHandler(IMapper mapper, IPermissionTypeRepository permissionTypeRepository)
        {
            _mapper = mapper;
            _permissionTypeRepository = permissionTypeRepository;
        }

        public async Task<PermissionTypeDto> Handle(GetPermissionTypeDetailRequest request, CancellationToken cancellationToken)
        {
            var permissionType = await _permissionTypeRepository.Get(request.Id);
            return _mapper.Map<PermissionTypeDto>(permissionType);
        }
    }
}
