﻿using AutoMapper;
using MediatR;
using N5.PermissionsManagement.Application.DTOs.PermissionType;
using N5.PermissionsManagement.Application.Features.PermissionTypes.Requests.Queries;
using N5.PermissionsManagement.UnitOfWork.Interface;

namespace N5.PermissionsManagement.Application.Features.PermissionTypes.Handlers.Queries
{
    public class GetPermissionTypeListRequestHandler : IRequestHandler<GetPermissionTypeListRequest, List<PermissionTypeDto>>
    {
        private readonly IPermissionTypeRepository _permissionTypeRepository;
        private readonly IMapper _mapper;

        public GetPermissionTypeListRequestHandler(IMapper mapper, IPermissionTypeRepository permissionTypeRepository)
        {
            _mapper = mapper;
            _permissionTypeRepository = permissionTypeRepository;
        }

        public async Task<List<PermissionTypeDto>> Handle(GetPermissionTypeListRequest request, CancellationToken cancellationToken)
        {
            var permissionTypes = await _permissionTypeRepository.GetAll();
            return _mapper.Map<List<PermissionTypeDto>>(permissionTypes);
        }
    }
}
