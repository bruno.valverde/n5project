﻿using AutoMapper;
using MediatR;
using N5.PermissionsManagement.Application.DTOs.PermissionType.Validators;
using N5.PermissionsManagement.Application.Exceptions;
using N5.PermissionsManagement.Application.Features.PermissionTypes.Requests.Commands;
using N5.PermissionsManagement.UnitOfWork.Interface;

namespace N5.PermissionsManagement.Application.Features.PermissionTypes.Handlers.Commands
{
    public class UpdatePermissionTypeCommandHandler : IRequestHandler<UpdatePermissionTypeCommand, Unit>
    {
        private readonly IUnitOfWorkRepository _unitOfWorkRepository;
        private readonly IMapper _mapper;

        public UpdatePermissionTypeCommandHandler(IMapper mapper, IUnitOfWorkRepository unitOfWorkRepository)
        {
            _mapper = mapper;
            _unitOfWorkRepository = unitOfWorkRepository;
        }

        public async Task<Unit> Handle(UpdatePermissionTypeCommand request, CancellationToken cancellationToken)
        {
            var validator = new UpdatePermissionTypeDtoValidator();
            var validationResult = await validator.ValidateAsync(request.PermissionTypeDto);

            if (validationResult.IsValid == false)
                throw new ValidationException(validationResult);

            var permissionType = await _unitOfWorkRepository.PermissionTypeRepository.Get(request.PermissionTypeDto.Id);

            if (permissionType is null)
                throw new NotFoundException(nameof(permissionType), request.PermissionTypeDto.Id);

            _mapper.Map(request.PermissionTypeDto, permissionType);

            await _unitOfWorkRepository.PermissionTypeRepository.Update(permissionType);
            await _unitOfWorkRepository.Save();

            return Unit.Value;
        }
    }
}
