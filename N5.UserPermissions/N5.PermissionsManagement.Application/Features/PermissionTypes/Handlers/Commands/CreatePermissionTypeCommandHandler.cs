﻿using AutoMapper;
using MediatR;
using N5.PermissionsManagement.Application.DTOs.PermissionType.Validators;
using N5.PermissionsManagement.Application.Features.PermissionTypes.Requests.Commands;
using N5.PermissionsManagement.Application.Responses;
using N5.PermissionsManagement.Domain;
using N5.PermissionsManagement.UnitOfWork.Interface;

namespace N5.PermissionsManagement.Application.Features.PermissionTypes.Handlers.Commands
{
    public class CreatePermissionTypeCommandHandler : IRequestHandler<CreatePermissionTypeCommand, BaseCommandResponse>
    {
        private readonly IUnitOfWorkRepository _unitOfWorkRepository;
        private readonly IMapper _mapper;

        public CreatePermissionTypeCommandHandler(IMapper mapper, IUnitOfWorkRepository unitOfWorkRepository)
        {
            _mapper = mapper;
            _unitOfWorkRepository = unitOfWorkRepository;
        }

        public async Task<BaseCommandResponse> Handle(CreatePermissionTypeCommand request, CancellationToken cancellationToken)
        {
            var response = new BaseCommandResponse();
            var validator = new CreatePermissionTypeDtoValidator();
            var validationResult = await validator.ValidateAsync(request.PermissionTypeDto);

            if (validationResult.IsValid == false)
            {
                response.Success = false;
                response.Message = "Creation Failed";
                response.Errors = validationResult.Errors.Select(q => q.ErrorMessage).ToList();
            }
            else
            {
                var permissionType = _mapper.Map<PermissionType>(request.PermissionTypeDto);

                permissionType = await _unitOfWorkRepository.PermissionTypeRepository.Add(permissionType);
                await _unitOfWorkRepository.Save();

                response.Success = true;
                response.Message = "Creation Successful";
                response.Id = permissionType.Id;
            }

            return response;
        }
    }
}
