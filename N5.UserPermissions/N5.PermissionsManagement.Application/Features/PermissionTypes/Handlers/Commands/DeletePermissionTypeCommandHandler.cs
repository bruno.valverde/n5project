﻿using AutoMapper;
using MediatR;
using N5.PermissionsManagement.Application.Exceptions;
using N5.PermissionsManagement.Application.Features.PermissionTypes.Requests.Commands;
using N5.PermissionsManagement.Domain;
using N5.PermissionsManagement.UnitOfWork.Interface;

namespace N5.PermissionsManagement.Application.Features.PermissionTypes.Handlers.Commands
{
    public class DeletePermissionTypeCommandHandler : IRequestHandler<DeletePermissionTypeCommand>
    {
        private readonly IUnitOfWorkRepository _unitOfWorkRepository;
        private readonly IMapper _mapper;

        public DeletePermissionTypeCommandHandler(IMapper mapper, IUnitOfWorkRepository unitOfWorkRepository)
        {
            _mapper = mapper;
            _unitOfWorkRepository = unitOfWorkRepository;
        }

        public async Task<Unit> Handle(DeletePermissionTypeCommand request, CancellationToken cancellationToken)
        {
            var permissionType = await _unitOfWorkRepository.PermissionTypeRepository.Get(request.Id);

            if (permissionType == null)
                throw new NotFoundException(nameof(PermissionType), request.Id);

            await _unitOfWorkRepository.PermissionTypeRepository.Delete(permissionType);
            await _unitOfWorkRepository.Save();

            return Unit.Value;
        }
    }
}
