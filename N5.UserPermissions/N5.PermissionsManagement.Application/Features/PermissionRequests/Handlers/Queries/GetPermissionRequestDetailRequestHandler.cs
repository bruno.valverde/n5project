﻿using AutoMapper;
using MediatR;
using N5.PermissionsManagement.Application.DTOs.Employee;
using N5.PermissionsManagement.Application.DTOs.PermissionRequest;
using N5.PermissionsManagement.Application.Features.PermissionRequests.Requests.Queries;
using N5.PermissionsManagement.UnitOfWork.Interface;

namespace N5.PermissionsManagement.Application.Features.PermissionRequests.Handlers.Queries
{
    public class GetPermissionRequestDetailRequestHandler : IRequestHandler<GetPermissionRequestDetailRequest, PermissionRequestDto>
    {
        private readonly IPermissionRequestRepository _permissionRequestRepository;
        private readonly IMapper _mapper;
        private readonly IEmployeeRepository _employeeRepository;

        public GetPermissionRequestDetailRequestHandler(IPermissionRequestRepository permissionRequestRepository, IMapper mapper, IEmployeeRepository employeeRepository)
        {
            _permissionRequestRepository = permissionRequestRepository;
            _mapper = mapper;
            _employeeRepository = employeeRepository;
        }

        public async Task<PermissionRequestDto> Handle(GetPermissionRequestDetailRequest request, CancellationToken cancellationToken)
        {
            var permissionRequest = _mapper.Map<PermissionRequestDto>(await _permissionRequestRepository.GetPermissionRequestWithDetails(request.Id));
            permissionRequest.Employee = _mapper.Map<EmployeeDto>(await _employeeRepository.GetEmployee(permissionRequest.RequestingEmployeeId));
            return permissionRequest;
        }
    }
}
