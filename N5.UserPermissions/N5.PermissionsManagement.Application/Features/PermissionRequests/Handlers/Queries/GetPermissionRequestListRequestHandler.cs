﻿using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using N5.PermissionsManagement.Application.DTOs.Employee;
using N5.PermissionsManagement.Application.DTOs.PermissionRequest;
using N5.PermissionsManagement.Application.Features.PermissionRequests.Requests.Queries;
using N5.PermissionsManagement.UnitOfWork.Interface;

namespace N5.PermissionsManagement.Application.Features.PermissionRequests.Handlers.Queries
{
    public class GetPermissionRequestListRequestHandler : IRequestHandler<GetPermissionRequestListRequest, List<PermissionRequestListDto>>
    {
        private readonly IPermissionRequestRepository _permissionRequestRepository;
        private readonly IMapper _mapper;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly ILogger<GetPermissionRequestListRequestHandler> _logger;

        public GetPermissionRequestListRequestHandler(IEmployeeRepository employeeRepository,
            IMapper mapper,
            IPermissionRequestRepository permissionRequestRepository,
            ILogger<GetPermissionRequestListRequestHandler> logger)
        {
            _employeeRepository = employeeRepository;
            _mapper = mapper;
            _permissionRequestRepository = permissionRequestRepository;
            _logger = logger;
        }

        public async Task<List<PermissionRequestListDto>> Handle(GetPermissionRequestListRequest request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("Get list permission requested");
            var permissionRequests = await _permissionRequestRepository.GetPermissionRequestsWithDetails();
            var requests = _mapper.Map<List<PermissionRequestListDto>>(permissionRequests);
            foreach (var req in requests)
            {
                req.Employee = _mapper.Map<EmployeeDto>(await _employeeRepository.GetEmployee(req.RequestingEmployeeId));
            }
            
            return requests;
        }
    }
}
