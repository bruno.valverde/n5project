﻿using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using N5.PermissionsManagement.Application.DTOs.PermissionRequest.Validators;
using N5.PermissionsManagement.Application.Exceptions;
using N5.PermissionsManagement.Application.Features.PermissionRequests.Requests.Commands;
using N5.PermissionsManagement.UnitOfWork.Interface;

namespace N5.PermissionsManagement.Application.Features.PermissionRequests.Handlers.Commands
{
    public class UpdatePermissionRequestCommandHandler : IRequestHandler<UpdatePermissionRequestCommand, Unit>
    {
        private readonly IUnitOfWorkRepository _unitOfWorkRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<UpdatePermissionRequestCommandHandler> _logger;

        public UpdatePermissionRequestCommandHandler(IMapper mapper, 
            IUnitOfWorkRepository unitOfWorkRepository, 
            ILogger<UpdatePermissionRequestCommandHandler> logger)
        {
            _mapper = mapper;
            _unitOfWorkRepository = unitOfWorkRepository;
            _logger = logger;
        }

        public async Task<Unit> Handle(UpdatePermissionRequestCommand request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("Update permission requested", request.PermissionRequestDto);
            var permissionRequest = await _unitOfWorkRepository.PermissionRequestRepository.Get(request.Id);

            if (permissionRequest is null)
                throw new NotFoundException(nameof(permissionRequest), request.Id);

            if (request.PermissionRequestDto != null)
            {
                var validator = new UpdatePermissionRequestDtoValidator(_unitOfWorkRepository.PermissionTypeRepository, _unitOfWorkRepository.EmployeeRepository);
                var validationResult = await validator.ValidateAsync(request.PermissionRequestDto, CancellationToken.None);

                if (validationResult.IsValid == false)
                    throw new ValidationException(validationResult);

                _mapper.Map(request.PermissionRequestDto, permissionRequest);

                await _unitOfWorkRepository.PermissionRequestRepository.Update(permissionRequest);
                await _unitOfWorkRepository.Save();

                _logger.LogInformation("Update request successfully", request.PermissionRequestDto);
            }
            else if (request.ChangePermissionRequestApprovalDto != null)
            {
                await _unitOfWorkRepository.PermissionRequestRepository.ChangeApprovalStatus(permissionRequest, request.ChangePermissionRequestApprovalDto.Approved);
                if (request.ChangePermissionRequestApprovalDto.Approved)
                {
                    var allocation = await _unitOfWorkRepository.PermissionAllocationRepository.GetUserAllocations(permissionRequest.RequestingEmployeeId, permissionRequest.PermissionTypeId);
                    int daysRequested = (int)(permissionRequest.EndDate - permissionRequest.StartDate).TotalDays;

                    allocation.NumberOfDays -= daysRequested;

                    await _unitOfWorkRepository.PermissionAllocationRepository.Update(allocation);
                }

                await _unitOfWorkRepository.Save();
            }

            return Unit.Value;
        }
    }
}
