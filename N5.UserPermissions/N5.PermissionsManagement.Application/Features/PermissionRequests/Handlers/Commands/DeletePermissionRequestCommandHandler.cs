﻿using AutoMapper;
using MediatR;
using N5.PermissionsManagement.Application.Exceptions;
using N5.PermissionsManagement.Application.Features.PermissionRequests.Requests.Commands;
using N5.PermissionsManagement.Domain;
using N5.PermissionsManagement.UnitOfWork.Interface;


namespace N5.PermissionsManagement.Application.Features.PermissionRequests.Handlers.Commands
{
    public class DeletePermissionRequestCommandHandler : IRequestHandler<DeletePermissionRequestCommand>
    {
        private readonly IUnitOfWorkRepository _unitOfWorkRepository;
        private readonly IMapper _mapper;

        public DeletePermissionRequestCommandHandler(IUnitOfWorkRepository unitOfWorkRepository, IMapper mapper)
        {
            _unitOfWorkRepository = unitOfWorkRepository;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(DeletePermissionRequestCommand request, CancellationToken cancellationToken)
        {
            var permissionRequest = await _unitOfWorkRepository.PermissionRequestRepository.Get(request.Id);

            if (permissionRequest == null)
                throw new NotFoundException(nameof(PermissionRequest), request.Id);

            await _unitOfWorkRepository.PermissionRequestRepository.Delete(permissionRequest);
            await _unitOfWorkRepository.Save();
            return Unit.Value;
        }
    }
}
