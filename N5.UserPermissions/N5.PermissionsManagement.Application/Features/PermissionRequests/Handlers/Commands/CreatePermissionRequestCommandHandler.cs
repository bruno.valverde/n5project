﻿using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using N5.PermissionsManagement.Application.DTOs.PermissionRequest.Validators;
using N5.PermissionsManagement.Application.Features.PermissionRequests.Requests.Commands;
using N5.PermissionsManagement.Application.Responses;
using N5.PermissionsManagement.Domain;
using N5.PermissionsManagement.UnitOfWork.Interface;

namespace N5.PermissionsManagement.Application.Features.PermissionRequests.Handlers.Commands
{
    public class CreatePermissionRequestCommandHandler : IRequestHandler<CreatePermissionRequestCommand, BaseCommandResponse>
    {
        private readonly IUnitOfWorkRepository _unitOfWorkRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<CreatePermissionRequestCommandHandler> _logger;

        public CreatePermissionRequestCommandHandler(IUnitOfWorkRepository unitOfWorkRepository, 
            IMapper mapper, 
            ILogger<CreatePermissionRequestCommandHandler> logger)
        {
            _unitOfWorkRepository = unitOfWorkRepository;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<BaseCommandResponse> Handle(CreatePermissionRequestCommand request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("Create Request Permission", request.PermissionRequestDto);

            var response = new BaseCommandResponse();
            var validator = new CreatePermissionRequestDtoValidator(_unitOfWorkRepository.PermissionTypeRepository, _unitOfWorkRepository.EmployeeRepository);
            var validationResult = await validator.ValidateAsync(request.PermissionRequestDto, CancellationToken.None);
            
            var allocation = await _unitOfWorkRepository.PermissionAllocationRepository
                .GetUserAllocations(request.PermissionRequestDto.EmployeeId, request.PermissionRequestDto.PermissionTypeId);

            if (allocation is null)
            {
                validationResult.Errors.Add(new FluentValidation.Results.ValidationFailure(nameof(request.PermissionRequestDto.PermissionTypeId),
                    "You do not have any allocations for this permission type."));
            }
            else
            {
                int daysRequested = (int)(request.PermissionRequestDto.EndDate - request.PermissionRequestDto.StartDate).TotalDays;
                if (daysRequested > allocation.NumberOfDays)
                {
                    validationResult.Errors.Add(new FluentValidation.Results.ValidationFailure(
                        nameof(request.PermissionRequestDto.EndDate), "You do not have enough days for this request"));
                }
            }

            if (validationResult.IsValid == false)
            {
                response.Success = false;
                response.Message = "Request Failed";
                response.Errors = validationResult.Errors.Select(q => q.ErrorMessage).ToList();
            }
            else
            {
                var permissionRequest = _mapper.Map<PermissionRequest>(request.PermissionRequestDto);
                permissionRequest.RequestingEmployeeId = request.PermissionRequestDto.EmployeeId;
                permissionRequest = await _unitOfWorkRepository.PermissionRequestRepository.Add(permissionRequest);
                await _unitOfWorkRepository.Save();

                response.Success = true;
                response.Message = "Request Created Successfully";
                response.Id = permissionRequest.Id;

                _logger.LogInformation("Request Created Successfully", request.PermissionRequestDto);
            }

            return response;
        }
    }
}
