﻿using MediatR;
using N5.PermissionsManagement.Application.DTOs.PermissionRequest;

namespace N5.PermissionsManagement.Application.Features.PermissionRequests.Requests.Queries
{
    public class GetPermissionRequestDetailRequest : IRequest<PermissionRequestDto>
    {
        public int Id { get; set; }
    }
}
