﻿using MediatR;
using N5.PermissionsManagement.Application.DTOs.PermissionRequest;
using N5.PermissionsManagement.Application.Responses;

namespace N5.PermissionsManagement.Application.Features.PermissionRequests.Requests.Commands
{
    public class CreatePermissionRequestCommand : IRequest<BaseCommandResponse>
    {
        public CreatePermissionRequestDto PermissionRequestDto { get; set; }
    }
}
