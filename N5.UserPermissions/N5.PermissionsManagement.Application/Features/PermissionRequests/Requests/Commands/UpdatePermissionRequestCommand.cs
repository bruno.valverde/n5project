﻿using MediatR;
using N5.PermissionsManagement.Application.DTOs.PermissionRequest;

namespace N5.PermissionsManagement.Application.Features.PermissionRequests.Requests.Commands
{
    public class UpdatePermissionRequestCommand : IRequest<Unit>
    {
        public int Id { get; set; }
        public UpdatePermissionRequestDto PermissionRequestDto { get; set; }
        public ChangePermissionRequestApprovalDto ChangePermissionRequestApprovalDto { get; set; }
    }
}
