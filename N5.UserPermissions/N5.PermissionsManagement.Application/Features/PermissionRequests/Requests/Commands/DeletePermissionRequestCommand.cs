﻿using MediatR;

namespace N5.PermissionsManagement.Application.Features.PermissionRequests.Requests.Commands
{
    public class DeletePermissionRequestCommand : IRequest
    {
        public int Id { get; set; }
    }
}
