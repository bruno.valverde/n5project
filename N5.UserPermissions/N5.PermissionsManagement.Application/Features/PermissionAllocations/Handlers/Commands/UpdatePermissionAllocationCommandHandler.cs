﻿using AutoMapper;
using MediatR;
using N5.PermissionsManagement.Application.DTOs.PermissionAllocation.Validators;
using N5.PermissionsManagement.Application.Exceptions;
using N5.PermissionsManagement.Application.Features.PermissionAllocations.Requests.Commands;
using N5.PermissionsManagement.UnitOfWork.Interface;

namespace N5.PermissionsManagement.Application.Features.PermissionAllocations.Handlers.Commands
{
    public class UpdatePermissionAllocationCommandHandler : IRequestHandler<UpdatePermissionAllocationCommand, Unit>
    {
        private readonly IUnitOfWorkRepository _unitOfWorkRepository;
        private readonly IMapper _mapper;

        public UpdatePermissionAllocationCommandHandler(IUnitOfWorkRepository unitOfWorkRepository, IMapper mapper)
        {
            _unitOfWorkRepository = unitOfWorkRepository;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(UpdatePermissionAllocationCommand request, CancellationToken cancellationToken)
        {
            var validator = new UpdatePermissionAllocationDtoValidator(_unitOfWorkRepository.PermissionTypeRepository);
            var validationResult = await validator.ValidateAsync(request.PermissionAllocationDto);

            if (validationResult.IsValid == false)
                throw new ValidationException(validationResult);

            var permissionAllocation = await _unitOfWorkRepository.PermissionAllocationRepository.Get(request.PermissionAllocationDto.Id);

            if (permissionAllocation is null)
                throw new NotFoundException(nameof(permissionAllocation), request.PermissionAllocationDto.Id);

            _mapper.Map(request.PermissionAllocationDto, permissionAllocation);

            await _unitOfWorkRepository.PermissionAllocationRepository.Update(permissionAllocation);
            await _unitOfWorkRepository.Save();
            return Unit.Value;
        }
    }
}
