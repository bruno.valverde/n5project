﻿using AutoMapper;
using MediatR;
using N5.PermissionsManagement.Application.Exceptions;
using N5.PermissionsManagement.Application.Features.PermissionAllocations.Requests.Commands;
using N5.PermissionsManagement.UnitOfWork.Interface;

namespace N5.PermissionsManagement.Application.Features.PermissionAllocations.Handlers.Commands
{
    public class DeletePermissionAllocationCommandHandler : IRequestHandler<DeletePermissionAllocationCommand>
    {
        private readonly IUnitOfWorkRepository _unitOfWorkRepository;
        private readonly IMapper _mapper;

        public DeletePermissionAllocationCommandHandler(IUnitOfWorkRepository unitOfWorkRepository, IMapper mapper)
        {
            _unitOfWorkRepository = unitOfWorkRepository;
            _mapper = mapper;
        }


        public async Task<Unit> Handle(DeletePermissionAllocationCommand request, CancellationToken cancellationToken)
        {
            var permissionAllocation = await _unitOfWorkRepository.PermissionAllocationRepository.Get(request.Id);

            if (permissionAllocation == null)
                throw new NotFoundException(nameof(permissionAllocation), request.Id);

            await _unitOfWorkRepository.PermissionAllocationRepository.Delete(permissionAllocation);
            await _unitOfWorkRepository.Save();
            return Unit.Value;
        }
    }
}
