﻿using AutoMapper;
using MediatR;
using N5.PermissionsManagement.Application.DTOs.PermissionAllocation.Validators;
using N5.PermissionsManagement.Application.Features.PermissionAllocations.Requests.Commands;
using N5.PermissionsManagement.Application.Responses;
using N5.PermissionsManagement.UnitOfWork.Interface;

namespace N5.PermissionsManagement.Application.Features.PermissionAllocations.Handlers.Commands
{
    public class CreatePermissionAllocationCommandHandler : IRequestHandler<CreatePermissionAllocationCommand, BaseCommandResponse>
    {
        private readonly IUnitOfWorkRepository _unitOfWork;
        private readonly IMapper _mapper;

        public CreatePermissionAllocationCommandHandler(IUnitOfWorkRepository unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<BaseCommandResponse> Handle(CreatePermissionAllocationCommand request, CancellationToken cancellationToken)
        {
            var response = new BaseCommandResponse();
            var validator = new CreatePermissionAllocationDtoValidator(_unitOfWork.PermissionTypeRepository);
            var validationResult = await validator.ValidateAsync(request.PermissionAllocationDto);

            if (validationResult.IsValid == false)
            {
                response.Success = false;
                response.Message = "Allocations Failed";
                response.Errors = validationResult.Errors.Select(q => q.ErrorMessage).ToList();
            }
            else
            {
                var permissionType = await _unitOfWork.PermissionTypeRepository.Get(request.PermissionAllocationDto.PermissionTypeId);
                var employees = await _unitOfWork.EmployeeRepository.GetEmployees();
                var period = DateTime.Now.Year;
                var allocations = new List<Domain.PermissionAllocation>();
                foreach (var emp in employees)
                {
                    if (await _unitOfWork.PermissionAllocationRepository.AllocationExists(emp.EmployeeId, permissionType.Id, period))
                        continue;
                    allocations.Add(new Domain.PermissionAllocation
                    {
                        EmployeeId = emp.EmployeeId,
                        PermissionTypeId = permissionType.Id,
                        NumberOfDays = permissionType.DefaultDays,
                        Period = period
                    });
                }

                await _unitOfWork.PermissionAllocationRepository.AddAllocations(allocations);
                await _unitOfWork.Save();
                response.Success = true;
                response.Message = "Allocations Successful";
            }

            return response;
        }
    }
}
