﻿using AutoMapper;
using MediatR;
using N5.PermissionsManagement.Application.DTOs.PermissionAllocation;
using N5.PermissionsManagement.Application.Features.PermissionAllocations.Requests.Queries;
using N5.PermissionsManagement.UnitOfWork.Interface;

namespace N5.PermissionsManagement.Application.Features.PermissionAllocations.Handlers.Queries
{
    public class GetPermissionAllocationDetailRequestHandler : IRequestHandler<GetPermissionAllocationDetailRequest, PermissionAllocationDto>
    {
        private readonly IPermissionAllocationRepository _permissionAllocationRepository;
        private readonly IMapper _mapper;


        public GetPermissionAllocationDetailRequestHandler(IPermissionAllocationRepository permissionAllocationRepository, IMapper mapper)
        {
            _permissionAllocationRepository = permissionAllocationRepository;
            _mapper = mapper;
        }

        public async Task<PermissionAllocationDto> Handle(GetPermissionAllocationDetailRequest request, CancellationToken cancellationToken)
        {
            var permissionAllocation = await _permissionAllocationRepository.GetPermissionAllocationWithDetails(request.Id);
            return _mapper.Map<PermissionAllocationDto>(permissionAllocation);
        }
    }
}
