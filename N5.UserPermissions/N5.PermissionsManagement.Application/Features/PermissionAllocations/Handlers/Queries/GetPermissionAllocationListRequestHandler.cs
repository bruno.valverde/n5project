﻿using AutoMapper;
using MediatR;
using N5.PermissionsManagement.Application.DTOs.Employee;
using N5.PermissionsManagement.Application.DTOs.PermissionAllocation;
using N5.PermissionsManagement.Application.Features.PermissionAllocations.Requests.Queries;
using N5.PermissionsManagement.UnitOfWork.Interface;

namespace N5.PermissionsManagement.Application.Features.PermissionAllocations.Handlers.Queries
{
    public class GetPermissionAllocationListRequestHandler : IRequestHandler<GetPermissionAllocationListRequest, List<PermissionAllocationDto>>
    {
        private readonly IPermissionAllocationRepository _permissionAllocationRepository;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IMapper _mapper;

        public GetPermissionAllocationListRequestHandler(IPermissionAllocationRepository permissionAllocationRepository, IMapper mapper, IEmployeeRepository employeeRepository)
        {
            _permissionAllocationRepository = permissionAllocationRepository;
            _mapper = mapper;
            _employeeRepository = employeeRepository;
        }

        public async Task<List<PermissionAllocationDto>> Handle(GetPermissionAllocationListRequest request, CancellationToken cancellationToken)
        {
            var permissionAllocations = await _permissionAllocationRepository.GetPermissionAllocationsWithDetails();
            var allocations = _mapper.Map<List<PermissionAllocationDto>>(permissionAllocations);
            foreach (var req in allocations)
            {
                req.Employee = _mapper.Map<EmployeeDto>(await _employeeRepository.GetEmployee(req.EmployeeId));
            }

            return allocations;
        }
    }
}
