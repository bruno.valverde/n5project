﻿using MediatR;
using N5.PermissionsManagement.Application.DTOs.PermissionAllocation;

namespace N5.PermissionsManagement.Application.Features.PermissionAllocations.Requests.Queries
{
    public class GetPermissionAllocationDetailRequest : IRequest<PermissionAllocationDto>
    {
        public int Id { get; set; }
    }
}
