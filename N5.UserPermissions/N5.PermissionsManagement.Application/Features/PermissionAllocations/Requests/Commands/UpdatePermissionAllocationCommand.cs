﻿using MediatR;
using N5.PermissionsManagement.Application.DTOs.PermissionAllocation;

namespace N5.PermissionsManagement.Application.Features.PermissionAllocations.Requests.Commands
{
    public class UpdatePermissionAllocationCommand : IRequest<Unit>
    {
        public UpdatePermissionAllocationDto PermissionAllocationDto { get; set; }
    }
}
