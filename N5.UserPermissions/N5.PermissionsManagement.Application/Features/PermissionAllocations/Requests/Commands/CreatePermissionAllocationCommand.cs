﻿using MediatR;
using N5.PermissionsManagement.Application.DTOs.PermissionAllocation;
using N5.PermissionsManagement.Application.Responses;

namespace N5.PermissionsManagement.Application.Features.PermissionAllocations.Requests.Commands
{
    public class CreatePermissionAllocationCommand : IRequest<BaseCommandResponse>
    {
        public CreatePermissionAllocationDto PermissionAllocationDto { get; set; }
    }
}
