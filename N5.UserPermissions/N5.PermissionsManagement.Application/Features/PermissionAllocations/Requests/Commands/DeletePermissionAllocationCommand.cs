﻿using MediatR;

namespace N5.PermissionsManagement.Application.Features.PermissionAllocations.Requests.Commands
{
    public class DeletePermissionAllocationCommand : IRequest
    {
        public int Id { get; set; }
    }
}
