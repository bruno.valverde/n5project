﻿using N5.PermissionsManagement.Domain.Common;

namespace N5.PermissionsManagement.Domain
{
    public class PermissionAllocation : BaseDomainPermissionEntity
    {
        public int NumberOfDays { get; set; }
        public PermissionType? PermisionType { get; set; }
        public int PermissionTypeId { get; set; }
        public int Period { get; set; }
        public int EmployeeId { get; set; }
    }
}
