﻿using N5.PermissionsManagement.Domain.Common;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace N5.PermissionsManagement.Domain
{
    public class Employee : BaseDomainUserEntity
    {
        [Key]
        public int EmployeeId { get; set; }

        [Column(TypeName ="varchar(10)")]
        public string EmployeeFile { get; set; } = string.Empty;
        public int SectorId { get; set; }

        [Column(TypeName = "date")]
        public DateTime StartWorkDate { get; set; }

    }
}
