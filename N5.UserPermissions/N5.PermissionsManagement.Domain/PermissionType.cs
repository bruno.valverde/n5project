﻿using N5.PermissionsManagement.Domain.Common;
using System.ComponentModel.DataAnnotations.Schema;

namespace N5.PermissionsManagement.Domain
{
    public class PermissionType : BaseDomainPermissionEntity
    {
        [Column(TypeName = "varchar(100)")]
        public string Name { get; set; } = string.Empty;
        public int DefaultDays { get; set; }
    }
}
