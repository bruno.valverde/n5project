﻿
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace N5.PermissionsManagement.Domain.Common
{
    public abstract class BaseDomainPermissionEntity
    {
        public int Id { get; set; }

        [Column(TypeName = "date")]
        public DateTime DateCreated { get; set; }

        [Column(TypeName = "varchar(50)")]
        public string? CreatedBy { get; set; }

        [Column(TypeName = "date")]
        public DateTime LastModifiedDate { get; set; }

        [Column(TypeName = "varchar(50)")]
        public string? LastModifiedBy { get; set; }
    }
}
