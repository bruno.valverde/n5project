﻿
using System.ComponentModel.DataAnnotations.Schema;

namespace N5.PermissionsManagement.Domain.Common
{
    public abstract class BaseDomainUserEntity
    {
        [Column(TypeName = "varchar(50)")]
        public string FisrtName { get; set; } = string.Empty;

        [Column(TypeName = "varchar(50)")]
        public string LastName { get; set; } = string.Empty;

        [Column(TypeName = "varchar(50)")]
        public string UserName { get; set; } = string.Empty;

        [Column(TypeName = "varchar(50)")]
        public string Email { get; set; } = string.Empty;
        public bool EmailConfirmed { get; set; }
        public string PasswordHash { get; set; } = string.Empty;

    }
}
