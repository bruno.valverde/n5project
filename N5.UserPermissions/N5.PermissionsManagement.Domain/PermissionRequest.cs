﻿using N5.PermissionsManagement.Domain.Common;
using System.ComponentModel.DataAnnotations.Schema;


namespace N5.PermissionsManagement.Domain
{
    public class PermissionRequest : BaseDomainPermissionEntity
    {
        [Column(TypeName = "date")]
        public DateTime StartDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime EndDate { get; set; }
        public PermissionType PermissionType { get; set; }
        public int PermissionTypeId { get; set; }

        [Column(TypeName = "date")]
        public DateTime DateRequested { get; set; }
        public string? RequestComments { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DateActioned { get; set; }
        public bool? Approved { get; set; }
        public bool Cancelled { get; set; }
        public int RequestingEmployeeId { get; set; }
    }
}
