﻿namespace N5.UserPermissions.Api.Helpers
{
    public static class Constants
    {
        public static readonly string REQUEST_GET = "Get Request Permission";
        public static readonly string REQUEST_PUT = "Update Request Permission";
        public static readonly string REQUEST_POST = "Create Request Permission";
    }
}
