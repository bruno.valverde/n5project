﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using N5.PermissionsManagement.Application.DTOs.PermissionRequest;
using N5.PermissionsManagement.Application.Features.PermissionRequests.Requests.Commands;
using N5.PermissionsManagement.Application.Features.PermissionRequests.Requests.Queries;
using N5.PermissionsManagement.Application.Responses;
using N5.UserPermissions.Api.Helpers;

namespace N5.UserPermissions.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PermissionRequestsController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ILogger<PermissionRequestDto> _logger;

        public PermissionRequestsController(IMediator mediator, ILogger<PermissionRequestDto> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        /// <summary>
        /// Get permission list of all request 
        /// </summary>
        /// <returns></returns>
        // GET: api/<PermissionRequestsController>
        [HttpGet]
        public async Task<ActionResult<List<PermissionRequestListDto>>> Get()
        {
            _logger.LogInformation("Method invoked {request}", Constants.REQUEST_GET);
            var permissionRequests = await _mediator.Send(new GetPermissionRequestListRequest());
            return Ok(permissionRequests);
        }

        /// <summary>
        /// create a request based on a user and permission type
        /// </summary>
        /// <param name="permissionRequest"></param>
        /// <returns></returns>
        // POST api/<PermissionRequestsController>
        [HttpPost]
        public async Task<ActionResult<BaseCommandResponse>> Post([FromBody] CreatePermissionRequestDto permissionRequest)
        {
            _logger.LogInformation("Method invoked {request}", Constants.REQUEST_POST);
            var command = new CreatePermissionRequestCommand { PermissionRequestDto = permissionRequest };
            var repsonse = await _mediator.Send(command);
            return Ok(repsonse);
        }

        /// <summary>
        /// modifies the request for a permission for a given user and type of permission
        /// </summary>
        /// <param name="id">Permission Request ID</param>
        /// <param name="permissionRequest">the PermissionTypeId and EmployeeId must exists</param>
        /// <returns></returns>
        // PUT api/<PermissionRequestsController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody] UpdatePermissionRequestDto permissionRequest)
        {
            _logger.LogInformation("Method invoked {request}", Constants.REQUEST_PUT);
            var command = new UpdatePermissionRequestCommand { Id = id, PermissionRequestDto = permissionRequest };
            await _mediator.Send(command);
            return Ok();
        }

    }
}
