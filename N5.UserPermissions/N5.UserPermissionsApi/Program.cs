using N5.PermissionsManagement.Application;
using N5.PermissionsManagement.UnitOfWork.SqlServer;
using N5.UserPermissions.Api.Extensions;
using N5.UserPermissions.Api.Middleware;


var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.ConfigureLogger();
builder.Services.ConfigureApplicationServices();
builder.Services.ConfigurePersistenceServices(builder.Configuration);

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddCors(o =>
{
    o.AddPolicy("CorsPolicy",
        build => build.AllowAnyOrigin()
        .AllowAnyMethod()
        .AllowAnyHeader());
});

var app = builder.Build();

// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
    app.UseSwagger();
    app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "N5.PermissionManagement.Api v1"));
//}


app.MigrateDatabase<PermissionManagementDbContext>((context, services) =>
{
    var logger = services.GetService<ILogger<PermissionManagementDbContext>>();
});

app.UseMiddleware<ExceptionMiddleware>();

app.UseHttpsRedirection();

app.UseAuthorization();

app.UseCors("CorsPolicy");

app.MapControllers();

app.Run();
