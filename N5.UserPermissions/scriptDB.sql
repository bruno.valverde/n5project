USE [master]
GO
/****** Object:  Database [n5_permissionmanagement_db]    Script Date: 22/5/2022 02:40:12 ******/
CREATE DATABASE [n5_permissionmanagement_db]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'n5_permissionmanagement_db', FILENAME = N'B:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\n5_permissionmanagement_db.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'n5_permissionmanagement_db_log', FILENAME = N'B:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\n5_permissionmanagement_db_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [n5_permissionmanagement_db] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [n5_permissionmanagement_db].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [n5_permissionmanagement_db] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [n5_permissionmanagement_db] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [n5_permissionmanagement_db] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [n5_permissionmanagement_db] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [n5_permissionmanagement_db] SET ARITHABORT OFF 
GO
ALTER DATABASE [n5_permissionmanagement_db] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [n5_permissionmanagement_db] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [n5_permissionmanagement_db] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [n5_permissionmanagement_db] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [n5_permissionmanagement_db] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [n5_permissionmanagement_db] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [n5_permissionmanagement_db] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [n5_permissionmanagement_db] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [n5_permissionmanagement_db] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [n5_permissionmanagement_db] SET  ENABLE_BROKER 
GO
ALTER DATABASE [n5_permissionmanagement_db] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [n5_permissionmanagement_db] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [n5_permissionmanagement_db] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [n5_permissionmanagement_db] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [n5_permissionmanagement_db] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [n5_permissionmanagement_db] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [n5_permissionmanagement_db] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [n5_permissionmanagement_db] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [n5_permissionmanagement_db] SET  MULTI_USER 
GO
ALTER DATABASE [n5_permissionmanagement_db] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [n5_permissionmanagement_db] SET DB_CHAINING OFF 
GO
ALTER DATABASE [n5_permissionmanagement_db] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [n5_permissionmanagement_db] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [n5_permissionmanagement_db] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [n5_permissionmanagement_db] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [n5_permissionmanagement_db] SET QUERY_STORE = OFF
GO
USE [n5_permissionmanagement_db]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 22/5/2022 02:40:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Employees]    Script Date: 22/5/2022 02:40:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employees](
	[EmployeeId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeFile] [varchar](10) NOT NULL,
	[SectorId] [int] NOT NULL,
	[StartWorkDate] [date] NOT NULL,
	[FisrtName] [varchar](50) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[UserName] [varchar](50) NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Employees] PRIMARY KEY CLUSTERED 
(
	[EmployeeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PermissionAllocations]    Script Date: 22/5/2022 02:40:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PermissionAllocations](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NumberOfDays] [int] NOT NULL,
	[PermissionTypeId] [int] NOT NULL,
	[Period] [int] NOT NULL,
	[EmployeeId] [int] NOT NULL,
	[DateCreated] [date] NOT NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastModifiedDate] [date] NOT NULL,
	[LastModifiedBy] [varchar](50) NULL,
 CONSTRAINT [PK_PermissionAllocations] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PermissionRequests]    Script Date: 22/5/2022 02:40:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PermissionRequests](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StartDate] [date] NOT NULL,
	[EndDate] [date] NOT NULL,
	[PermissionTypeId] [int] NOT NULL,
	[DateRequested] [date] NOT NULL,
	[RequestComments] [nvarchar](max) NULL,
	[DateActioned] [date] NULL,
	[Approved] [bit] NULL,
	[Cancelled] [bit] NOT NULL,
	[RequestingEmployeeId] [int] NOT NULL,
	[DateCreated] [date] NOT NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastModifiedDate] [date] NOT NULL,
	[LastModifiedBy] [varchar](50) NULL,
 CONSTRAINT [PK_PermissionRequests] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PermissionTypes]    Script Date: 22/5/2022 02:40:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PermissionTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[DefaultDays] [int] NOT NULL,
	[DateCreated] [date] NOT NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastModifiedDate] [date] NOT NULL,
	[LastModifiedBy] [varchar](50) NULL,
 CONSTRAINT [PK_PermissionTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20220521204212_InitialCreation', N'6.0.5')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20220521221411_SeedingTablesTest', N'6.0.5')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20220521222321_SeedingPermissionAllocation', N'6.0.5')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20220521223955_addPkToEntities', N'6.0.5')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20220521225833_keymodification', N'6.0.5')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20220521230217_updateKey', N'6.0.5')
GO
SET IDENTITY_INSERT [dbo].[Employees] ON 

INSERT [dbo].[Employees] ([EmployeeId], [EmployeeFile], [SectorId], [StartWorkDate], [FisrtName], [LastName], [UserName], [Email], [EmailConfirmed], [PasswordHash]) VALUES (1, N'AR000123', 1, CAST(N'2022-05-21' AS Date), N'employeeFirstName', N'employeeLastName', N'', N'employee@n5.com', 1, N'7D9C61732398AE7A24BDEBE0CE1C82B064B3C5F8DD0D850E9F3C9CC80CCE980F')
SET IDENTITY_INSERT [dbo].[Employees] OFF
GO
SET IDENTITY_INSERT [dbo].[PermissionAllocations] ON 

INSERT [dbo].[PermissionAllocations] ([Id], [NumberOfDays], [PermissionTypeId], [Period], [EmployeeId], [DateCreated], [CreatedBy], [LastModifiedDate], [LastModifiedBy]) VALUES (1, 15, 1, 2022, 1, CAST(N'0001-01-01' AS Date), NULL, CAST(N'0001-01-01' AS Date), NULL)
INSERT [dbo].[PermissionAllocations] ([Id], [NumberOfDays], [PermissionTypeId], [Period], [EmployeeId], [DateCreated], [CreatedBy], [LastModifiedDate], [LastModifiedBy]) VALUES (2, 10, 2, 2022, 1, CAST(N'0001-01-01' AS Date), NULL, CAST(N'0001-01-01' AS Date), NULL)
SET IDENTITY_INSERT [dbo].[PermissionAllocations] OFF
GO
SET IDENTITY_INSERT [dbo].[PermissionRequests] ON 

INSERT [dbo].[PermissionRequests] ([Id], [StartDate], [EndDate], [PermissionTypeId], [DateRequested], [RequestComments], [DateActioned], [Approved], [Cancelled], [RequestingEmployeeId], [DateCreated], [CreatedBy], [LastModifiedDate], [LastModifiedBy]) VALUES (1, CAST(N'2022-05-21' AS Date), CAST(N'2022-05-27' AS Date), 1, CAST(N'0001-01-01' AS Date), N'change end date', NULL, NULL, 0, 1, CAST(N'2022-05-21' AS Date), N'SYSTEM', CAST(N'2022-05-21' AS Date), N'SYSTEM')
SET IDENTITY_INSERT [dbo].[PermissionRequests] OFF
GO
SET IDENTITY_INSERT [dbo].[PermissionTypes] ON 

INSERT [dbo].[PermissionTypes] ([Id], [Name], [DefaultDays], [DateCreated], [CreatedBy], [LastModifiedDate], [LastModifiedBy]) VALUES (1, N'Vacation', 10, CAST(N'0001-01-01' AS Date), NULL, CAST(N'0001-01-01' AS Date), NULL)
INSERT [dbo].[PermissionTypes] ([Id], [Name], [DefaultDays], [DateCreated], [CreatedBy], [LastModifiedDate], [LastModifiedBy]) VALUES (2, N'Sick', 3, CAST(N'0001-01-01' AS Date), NULL, CAST(N'0001-01-01' AS Date), NULL)
SET IDENTITY_INSERT [dbo].[PermissionTypes] OFF
GO
/****** Object:  Index [IX_PermissionAllocations_PermissionTypeId]    Script Date: 22/5/2022 02:40:12 ******/
CREATE NONCLUSTERED INDEX [IX_PermissionAllocations_PermissionTypeId] ON [dbo].[PermissionAllocations]
(
	[PermissionTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_PermissionRequests_PermissionTypeId]    Script Date: 22/5/2022 02:40:12 ******/
CREATE NONCLUSTERED INDEX [IX_PermissionRequests_PermissionTypeId] ON [dbo].[PermissionRequests]
(
	[PermissionTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PermissionAllocations]  WITH CHECK ADD  CONSTRAINT [FK_PermissionAllocations_PermissionTypes_PermissionTypeId] FOREIGN KEY([PermissionTypeId])
REFERENCES [dbo].[PermissionTypes] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PermissionAllocations] CHECK CONSTRAINT [FK_PermissionAllocations_PermissionTypes_PermissionTypeId]
GO
ALTER TABLE [dbo].[PermissionRequests]  WITH CHECK ADD  CONSTRAINT [FK_PermissionRequests_PermissionTypes_PermissionTypeId] FOREIGN KEY([PermissionTypeId])
REFERENCES [dbo].[PermissionTypes] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PermissionRequests] CHECK CONSTRAINT [FK_PermissionRequests_PermissionTypes_PermissionTypeId]
GO
USE [master]
GO
ALTER DATABASE [n5_permissionmanagement_db] SET  READ_WRITE 
GO
