﻿using Moq;
using N5.PermissionsManagement.UnitOfWork.Interface;

namespace N5.UserPermission.Api.Tests.Mocks
{
    public static class MockPermissionRequestRepository
    {
        public static Mock<IPermissionRequestRepository> GetPermissionRequestRepository()
        {
            var permissionRequests = new List<PermissionsManagement.Domain.PermissionRequest>
            {
                new PermissionsManagement.Domain.PermissionRequest
                {
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now,
                    PermissionTypeId = 1,
                    RequestComments = "test",
                    RequestingEmployeeId = 1
                }
            };

            var mockRepo = new Mock<IPermissionRequestRepository>();

            mockRepo.Setup(r => r.GetAll()).ReturnsAsync(permissionRequests);

            mockRepo.Setup(r => r.Add(It.IsAny<PermissionsManagement.Domain.PermissionRequest>())).ReturnsAsync((PermissionsManagement.Domain.PermissionRequest permissionRequest) =>
            {
                permissionRequests.Add(permissionRequest);
                return permissionRequest;
            });

            return mockRepo;
        }
    }
}
