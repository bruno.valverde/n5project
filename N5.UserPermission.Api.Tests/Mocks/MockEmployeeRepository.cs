﻿using Moq;
using N5.PermissionsManagement.Domain;
using N5.PermissionsManagement.UnitOfWork.Interface;

namespace N5.UserPermission.Api.Tests.Mocks
{
    public static class MockEmployeeRepository
    {
        public static Mock<IEmployeeRepository> GetEmployeeRepository()
        {
            var employees = new List<Employee>
            {
                new Employee
                {
                    EmployeeId = 1,
                    FisrtName = "employeeFirstName",
                    LastName = "employeeLastName",
                    Email = "employee@n5.com",
                    EmailConfirmed = true,
                    PasswordHash = "7D9C61732398AE7A24BDEBE0CE1C82B064B3C5F8DD0D850E9F3C9CC80CCE980F",
                    EmployeeFile = "AR000123",
                    SectorId = 1,
                    StartWorkDate = DateTime.Now
                }
            };

            var mockRepo = new Mock<IEmployeeRepository>();

            mockRepo.Setup(r => r.GetAll()).ReturnsAsync(employees);

            mockRepo.Setup(r => r.Add(It.IsAny<Employee>())).ReturnsAsync((Employee employee) =>
            {
                employees.Add(employee);
                return employee;
            });

            return mockRepo;

        }
    }
}
