﻿using Moq;
using N5.PermissionsManagement.Domain;
using N5.PermissionsManagement.UnitOfWork.Interface;

namespace N5.UserPermission.Api.Tests.Mocks
{
    public static class MockPermissionAllocationRepository
    {
        public static Mock<IPermissionAllocationRepository> GetPermissionAllocationRepository()
        {
            var permissionAllocations = new List<PermissionAllocation>
            {
              new PermissionAllocation
                {
                    Id = 1,
                    NumberOfDays = 15,
                    PermissionTypeId = 1,
                    Period = 2022,
                    EmployeeId = 1
                },
                new PermissionAllocation
                {
                    Id = 2,
                    NumberOfDays = 10,
                    PermissionTypeId = 2,
                    Period = 2022,
                    EmployeeId = 1
                }
            };

            var mockRepo = new Mock<IPermissionAllocationRepository>();

            mockRepo.Setup(r => r.GetAll()).ReturnsAsync(permissionAllocations);

            mockRepo.Setup(r => r.Add(It.IsAny<PermissionAllocation>())).ReturnsAsync((PermissionAllocation permissionAllocation) =>
            {
                permissionAllocations.Add(permissionAllocation);
                return permissionAllocation;
            });

            return mockRepo;

        }
    }
}
