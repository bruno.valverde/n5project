﻿using Moq;
using N5.PermissionsManagement.Domain;
using N5.PermissionsManagement.UnitOfWork.Interface;

namespace N5.UserPermission.Api.Tests.Mocks
{
    public static class MockPermissionTypeRepository
    {
        public static Mock<IPermissionTypeRepository> GetPermissionTypeRepository()
        {
            var permissionsTypes = new List<PermissionType>
            {
                new PermissionType
                {
                    Id = 1,
                    DefaultDays = 10,
                    Name = "Test Vacation"
                },
                new PermissionType
                {
                    Id = 2,
                    DefaultDays = 15,
                    Name = "Test Sick"
                },
                new PermissionType
                {
                    Id = 3,
                    DefaultDays = 15,
                    Name = "Test Maternity"
                }
            };

            var mockRepo = new Mock<IPermissionTypeRepository>();

            mockRepo.Setup(r => r.GetAll()).ReturnsAsync(permissionsTypes);

            mockRepo.Setup(r => r.Add(It.IsAny<PermissionType>())).ReturnsAsync((PermissionType permissionType) =>
            {
                permissionsTypes.Add(permissionType);
                return permissionType;
            });

            return mockRepo;
        }
    }
}
