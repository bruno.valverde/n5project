﻿using Moq;
using N5.PermissionsManagement.UnitOfWork.Interface;

namespace N5.UserPermission.Api.Tests.Mocks
{
    public static class MockUnitOfWorkRepository
    {
        public static Mock<IUnitOfWorkRepository> GetUnitOfWorkRepository()
        {
            var mockUow = new Mock<IUnitOfWorkRepository>();
            var mockPermissionTypeRepo = MockPermissionTypeRepository.GetPermissionTypeRepository();
            var mockPermissionRequestRepo = MockPermissionRequestRepository.GetPermissionRequestRepository();
            var mockPermissionAllocationRepo = MockPermissionAllocationRepository.GetPermissionAllocationRepository();
            var mockEmployeeRepo = MockEmployeeRepository.GetEmployeeRepository();

            mockUow.Setup(r => r.PermissionTypeRepository).Returns(mockPermissionTypeRepo.Object);
            mockUow.Setup(r => r.PermissionRequestRepository).Returns(mockPermissionRequestRepo.Object);
            mockUow.Setup(r => r.PermissionAllocationRepository).Returns(mockPermissionAllocationRepo.Object);
            mockUow.Setup(r => r.EmployeeRepository).Returns(mockEmployeeRepo.Object);

            return mockUow;
        }
    }
}
