﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using Moq;
using N5.PermissionsManagement.Application.DTOs.PermissionRequest;
using N5.PermissionsManagement.Application.Features.PermissionRequests.Handlers.Queries;
using N5.PermissionsManagement.Application.Features.PermissionRequests.Requests.Queries;
using N5.PermissionsManagement.Application.Profiles;
using N5.PermissionsManagement.UnitOfWork.Interface;
using N5.UserPermission.Api.Tests.Mocks;
using Shouldly;
using Xunit;

namespace N5.UserPermission.Api.Tests.PermissionRequest.Queries
{
    public class GetPermissionRequestListRequestHandlerTests
    {
        private readonly IMapper _mapper;
        private readonly Mock<IPermissionRequestRepository> _mockRepoPermissionRequest;
        private readonly Mock<IEmployeeRepository> _mockRepoEmployee;
        private readonly ILogger<GetPermissionRequestListRequestHandler> _logger;

        public GetPermissionRequestListRequestHandlerTests()
        {
            _mockRepoPermissionRequest = MockPermissionRequestRepository.GetPermissionRequestRepository();
            _mockRepoEmployee = MockEmployeeRepository.GetEmployeeRepository();

            var mapperConfig = new MapperConfiguration(c =>
            {
                c.AddProfile<MappingProfile>();
            });

            _mapper = mapperConfig.CreateMapper();

            var mock = new Mock<ILogger<GetPermissionRequestListRequestHandler>>();
            _logger = mock.Object;
        }

        [Fact]
        public async Task GetPermissionRequestListTest()
        {
            var handler = new GetPermissionRequestListRequestHandler(_mockRepoEmployee.Object, _mapper, _mockRepoPermissionRequest.Object, _logger);

            var result = await handler.Handle(new GetPermissionRequestListRequest(), CancellationToken.None);

            result.ShouldBeOfType<List<PermissionRequestListDto>>();

            result.Count.ShouldBe(0);

        }
    }
}
