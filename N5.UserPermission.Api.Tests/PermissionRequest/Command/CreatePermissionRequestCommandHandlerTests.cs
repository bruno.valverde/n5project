﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using Moq;
using N5.PermissionsManagement.Application.DTOs.PermissionRequest;
using N5.PermissionsManagement.Application.DTOs.PermissionType;
using N5.PermissionsManagement.Application.Features.PermissionRequests.Handlers.Commands;
using N5.PermissionsManagement.Application.Features.PermissionRequests.Requests.Commands;
using N5.PermissionsManagement.Application.Profiles;
using N5.PermissionsManagement.Application.Responses;
using N5.PermissionsManagement.UnitOfWork.Interface;
using Shouldly;
using Xunit;

namespace N5.UserPermission.Api.Tests.PermissionRequest.Command
{

    public class CreatePermissionRequestCommandHandlerTests
    {

        private readonly Mock<IUnitOfWorkRepository> _mockUow;
        private readonly IMapper _mapper;
        private readonly ILogger<CreatePermissionRequestCommandHandler> _logger;
        private readonly CreatePermissionRequestDto _permissionRequestDto;
        private readonly CreatePermissionTypeDto _permissionTypeDto;
        private readonly CreatePermissionRequestCommandHandler _handler;


        public CreatePermissionRequestCommandHandlerTests()
        {
            _mockUow = new Mock<IUnitOfWorkRepository>();
            var mapperConfig = new MapperConfiguration(c =>
            {
                c.AddProfile<MappingProfile>();
            });

            var mock = new Mock<ILogger<CreatePermissionRequestCommandHandler>>();
            _logger = mock.Object;

            _mapper = mapperConfig.CreateMapper();
            _handler = new CreatePermissionRequestCommandHandler(_mockUow.Object, _mapper, _logger);

            _permissionRequestDto = new CreatePermissionRequestDto()
            {
                StartDate = DateTime.Now,
                EndDate = DateTime.Now,
                PermissionTypeId = 1,
                RequestComments = "test2",
                EmployeeId = 1
            };

            _permissionTypeDto = new CreatePermissionTypeDto()
            {
                DefaultDays = 10,
                Name = "Test Vacation"
            };
        }

        [Fact]
        public async Task Valid_PermissionRequest_Added()
        {
            var result = await _handler.Handle(new CreatePermissionRequestCommand() { PermissionRequestDto = _permissionRequestDto }, CancellationToken.None);

            var permissionRequest = await _mockUow.Object.PermissionTypeRepository.GetAll();

            result.ShouldBeOfType<BaseCommandResponse>();

            permissionRequest.Count.ShouldBe(2);
        }

        [Fact]
        public async Task InValid_PermissionRequest_Added()
        {
            _permissionRequestDto.StartDate = DateTime.Now.AddDays(1);
            _permissionRequestDto.EndDate = DateTime.Now;

            var result = await _handler.Handle(new CreatePermissionRequestCommand() { PermissionRequestDto = _permissionRequestDto }, CancellationToken.None);

            var permissionRequests = await _mockUow.Object.PermissionRequestRepository.GetAll();

            permissionRequests.Count.ShouldBe(1);

            result.ShouldBeOfType<BaseCommandResponse>();

        }
    }
}
