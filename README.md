# N5Project

Tech Lead Challenge

## Description

El proyecto cuenta con 4 tablas _(Employee, Permission Request, Permision Type y Permission Allocation)_ las cuales están pensadas para la administración de los permisos a un usuario:

- Employee: Tabla que almacena los usuarios que pueden solicitar un permisos
- Permission Request: Solicitud de permiso de un empleado
- Permission Type: El tipo de permiso existente
- Permission Allocation: El permiso asignado a cada empleado _(puede tener más de un permiso asignado)_

Para fines prácticos, el proyecto cuenta con datos pre cargados para las tablas _Employee_, _Permission Allocation_ y _Permission Type_.

La web API tiene 3 endpoint para la tabla _Permission Request_

- GET: Devuelve todas las solicitudes (api/Get)
- POST: Crea una solicitud para un determinado empleado y un tipo de permiso (api/Post)
- PUT: Modifica la solicitud (api/put)

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/bruno.valverde/n5project.git
git branch -M main
git push -uf origin main
```

## Pre Requisitos

- [ ] Tener instalado Docker
- [ ] 4 GB como mínimo de memoria disponible

## Ejecución

Pasos para ejecutar el proyecto con Docker.

- [ ] Abrir una consola y ubicarse en la carpeta donde se encuentra el archivo _docker-compose_
- [ ] Ejecutar el siguiente comando:
```
docker-compose -f docker-compose.yml -f docker-compose.override.yml up -d
```

Cuando finalicen de descargar las imagenes, ingresar a la aplicación a través de la Url: https://localhost:52536/swagger/index.html

## Librerias

- Serilog: Para registrar los logs de los endpoints y enviarlos a ElasticSearch
- ElasticSearch: Guarda los registros generados por Serilog
- Kibana: Visualiza los registros guardados en ElasticSearch
- FluentValidation
- MediatR: Para la comunicación entre los endpoints y los servicios
- AutoMapper
- EntityFrameworkCore
- Moq
- Shouldly

## Patrones de diseño

- CQRS + MediatR
- Repository pattern
- Unit Of Work
